<?php
class Db{
    public static function conectar(){
            include'Conexion.php';
            return $dwes;
    }
    public static function registroUsuario($usuario, $pass, $nombre,$email, $nivel, $curso, $practicas, $tiempo, $pulsaciones, $ppmmax, $ip){
        $dwes=self::conectar();
        $sql="INSERT INTO user (usuario, pass, nombre,email, nivel, curso, practicas, tiempo, pulsaciones, ppmmax, ip) VALUES (:usuario, :pass, :nombre,:email, :nivel, :curso, :practicas, :tiempo, :pulsaciones, :ppmmax, :ip)";
        $resultado=$dwes->prepare($sql);
        
        $resultado->bindParam(':usuario',$usuario);
        $resultado->bindParam(':pass',$pass);
        $resultado->bindParam(':nombre',$nombre);
        $resultado->bindParam(':email',$email);
        $resultado->bindParam(':nivel',$nivel);
        $resultado->bindParam(':curso',$curso);
        $resultado->bindParam(':practicas',$practicas);
        $resultado->bindParam(':tiempo',$tiempo);
        $resultado->bindParam(':pulsaciones',$pulsaciones);
        $resultado->bindParam(':ppmmax',$ppmmax);
        $resultado->bindParam(':ip',$ip);
        $resultado->execute();
    }
    public static function loguin($usuario){
        $fila= array();
        $dwes=self::conectar();
        $sql="SELECT * FROM user WHERE usuario=:usuario";
        $resultado=$dwes->prepare($sql);
        $resultado->bindParam(':usuario',$usuario);
        $resultado->execute();
        $fila["error"]=true ;
        while ($fila2=$resultado->fetch(PDO::FETCH_OBJ)) {
                $fila["usuario"]=$fila2->usuario ;
                $fila["pass"]=$fila2->pass ;
                $fila["nivel"]=$fila2->nivel ;
                $fila["nombre"]=$fila2->nombre ;
                $fila["error"]=false ;
            }
        return $fila;
        
    }
    public static function introHistorico($usuario,$tiempo,$pulsaciones,$ppm,$ip,$fallos){
       $dwes=self::conectar();
        $sql="insert into historico values(:usuario,:tiempo,:pulsaciones,:ppm,:ip,:fallos)";
        $resultado=$dwes->prepare($sql);
        $resultado->bindParam(':usuario',$usuario);
        $resultado->bindParam(':tiempo',$tiempo);
        $resultado->bindParam(':pulsaciones',$pulsaciones);
        $resultado->bindParam(':ppm',$ppm);
        $resultado->bindParam(':ip',$ip);
        $resultado->bindParam(':fallos',$fallos);
        $resultado->execute(); 
    }
    public static function sacarHistorico($usuario){
       $dwes=self::conectar();
       $fila= array();
        //echo"bien";
        $sql="SELECT tiempo,pulsaciones,ppm,fallos FROM historico where usuario=:usuario;";
        echo $usuario;
        $resultado=$dwes->prepare($sql);
        $resultado->bindParam(':usuario',$usuario);
        $resultado->execute(); 
        $fila["error"]=true ;
        
        $t=0;
        while ($fila2=$resultado->fetch(PDO::FETCH_OBJ)) {
                $fila[$t]["tiempo"]=$fila2->tiempo ;
                $fila[$t]["pulsaciones"]=$fila2->pulsaciones ;
                $fila[$t]["ppm"]=$fila2->ppm ;
                $fila[$t]["fallos"]=$fila2->fallos ;
                
                $t++;
            }
        
        return $fila;
    }
    
    public static function pedirclave($p1,$p2){
        $dwes=self::conectar();
        $sql="SELECT pass FROM clave where par=:par COLLATE utf8_bin;";
        $resultado=$dwes->prepare($sql);
        $frase=$p1."=====$$$=====".$p2;
        $resultado->bindParam(':par',$frase);
        $resultado->execute();
        $salida="error";
            while ($fila=$resultado->fetch(PDO::FETCH_OBJ)) {
                $salida=$fila->pass ;
            }
        //salida contiene o error o el pass si existe
        if($salida=="error"){
            
            //genero una nueva clave
            $numerodado = rand(20,30); //nos dará 14-20
            $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890/!$%&=@#_+-"; //posibles caracteres a usar
            $numerodeletras=$numerodado; //numero de letras para generar el texto
            $cadena = ""; //variable para almacenar la cadena generada
            for($i=0;$i<$numerodeletras;$i++)
            {
                $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); /*Extraemos 1 caracter de los caracteres 
            entre el rango 0 a Numero de letras que tiene la cadena */
            }
            //guarda la clave
            $sql="insert into clave (par, pass) values(:frase,:clave) ;";
            $resultado=$dwes->prepare($sql);
            $resultado->bindParam(':frase',$frase);
            $resultado->bindParam(':clave',$cadena);
            $resultado->execute();
            $salida=$cadena;
        }
        $dwes=null;
        return $salida;
        
    }
    
}
?>

