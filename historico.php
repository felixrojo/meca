<?php
session_start();
?>
<?php
include "conex.php";
include "nombrebd.php";

mysql_select_db($_SESSION['basedatos']);
?>
<!DOCTYPE html>
<html><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="meca.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->
 <TITLE>Textos Mecanografía on-line.</TITLE>
<meta http-equiv="Content-Type" content="text/html; ISO-8859-1">
<META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
<META NAME="AUTHOR" CONTENT="Felix Rojo Trueba">
<META NAME="REPLY-TO" CONTENT="felix@cajon.es">
<LINK REV="made" href="mailto:felix@cajon.es">
<META NAME="DESCRIPTION" CONTENT="Textos para la practica mecanografica, mejorar su rapidez y rendimienteo mecanografiando.">
<META NAME="KEYWORDS" CONTENT="texto,practica,teclado,mecanografia,curso,test">
<META NAME="Resource-type" CONTENT="Document">
<META NAME="DateCreated" CONTENT="Mon, 26 May 2014 00:00:00 GMT+1">
<META NAME="robots" content="ALL">
 <!-- InstanceEndEditable -->
 <!-- icono -->
<LINK REL="Shortcut Icon" HREF="imagenes/icono.ico">
</head>
 <body>
 <div itemscope itemtype="http://schema.org/Person"id="todo">
   <div id="cabecera"><header><img src="imagenes/baner.jpg" width="1000" height="150" alt="Meca"></header></div>
   <br>
<div id="menu"><nav>
    <?php
    if(!isset($_SESSION['usuario']))
    {
    echo'
       <form name="form1" method="post" action="loguin_usuario.php">
    Usuario
           <input name="login" type="text" id="login" size="10">
    Contraseña
    <input name="contra"  type="password" id="contra" size="10">
    <input type="submit" name="Enviar" id="Enviar" value="Enviar">
    <a href="registro_de_usuarios.php">Registrate </a> // Recordar contraseña
    ';
    }else{
    echo'Gracias ' . $_SESSION['nombre'] . ' <a href="salida_usuario.php">Salir </a> // <a href="historico.php">Historico </a>';
    }
    ?>
<p><a href="index.php">Inicio</a> // <a href="postura_sentarse_ordenador.php">Postura</a> // <a href="mecanografia_colocacion_dedos.php">Colocación de los dedos</a> // <a href="lecciones_mecanografia.php">lecciones</a> // <a href="textos_mecanografia.php">Textos</a> // <a href="curso_de_mecanografia.php">Curso</a> // <a href="contacto.php">Contacto</a></p>
     <p>&nbsp;</p>
   </form>
   </nav></div>
   <div id="cuerpo"><section><!-- InstanceBeginEditable name="EditRegion1" -->
     <center><p><table  border="0" cellspacing="0" cellpadding="0">
             
  <tr  bgcolor="#CCCCCC">
    <td width="100">Tiempo.</td>
    <td width="100">Pulsaciones.</td>
    <td width="100">Ppm</td>
    <td width="100">Fallos</td>
    <td >Correcto</td>
  </tr>
  <?php
      require_once './include/DB.php';
      $datos=db::sacarHistorico($_SESSION['usuario']);
			//seleciono la tabla de noticias 
//			mysql_query("SET NAMES 'utf8'");
//			$pregunta = "SELECT * FROM textos  ORDER BY caracteres ";
//			$respuesta2=mysql_query ($pregunta);
                        $tiempoTotal=0;
                        $pulsasionesTotal=0;
                        foreach ($datos as $value) {
                           $t=0; 
                           if ($t%2==0){
				$color="#E8E8E8";
				}else{
                                    $color="#C5C4D9";
                                }
                                if (($value["pulsaciones"]/20)+1>$value["fallos"]){
                                    $img="imagenes/bien.png";
                                }else{
                                    $img="imagenes/error.png";
                                    
                                }
                                $tiempoTotal+=$value["tiempo"];
                                $pulsasionesTotal+=$value["pulsaciones"];
                                echo'
                                <tr bgcolor="'.$color.'">
                                        <td width="60">' .floor ($value["tiempo"]/60) .'M '.$value["tiempo"]%60 . 'S</td>
                                        <td width="60">' .$value["pulsaciones"]. '</td>
                                        <td width="60">' .$value["ppm"]. '</td>
                                        <td width="60">' .$value["fallos"]. '</td>
                                        <td ><img width="20px" src="'.$img.'"></td>
                                  </tr>
                                ';
                                
                        }
                        echo'
                                <tr bgcolor="#F5DA81">
                                        <td width="60">' .floor ($tiempoTotal/60) .'M '.$tiempoTotal%60 . 'S</td>
                                        <td width="60">' .$pulsasionesTotal. '</td>
                                        <td width="60"></td>
                                        <td width="60"></td>
                                        <td ></td>
                                  </tr>
                                ';
//			$t=0;
//			while ($fila =mysql_fetch_array($respuesta2))
//			{
//			$t++;
//			$id=$fila['id'];
//			$titulo= $fila['titulo'];
//			$descripcion=$fila['descripcion'];
//			$letras=$fila['letras'];
//			$caracteres=$fila['caracteres'];
//			
//			if ($t%2==0){
//				$color="#E8E8E8";
//				}else{$color="#C5C4D9";
//				
//					}
//			echo'
//			<tr bgcolor="'.$color.'">
//				<td width="60">' .$caracteres. '</td>
//				<td width="60">' .$titulo . '</td>
//				<td width="60">' .$letras. ' -> <a href="textos.php?id=' .$id. '">inicio</a></td>
//			  </tr>
//			';
//			}
//mysql_close()
?>
</table>
</p></center>
   <!-- InstanceEndEditable --></div>
   <div id="pie"><footer><div itemscope itemtype="http://schema.org/Person">Programado por: <span itemprop="name">Félix Rojo</span>, Contacto: <span itemprop="email">felix@cajon.es</span></div></footer></div>
 </div>
  
 </body>
<!-- InstanceEnd --></html>