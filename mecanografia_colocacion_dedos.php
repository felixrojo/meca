<?php
session_start();
?>
<!DOCTYPE html>
<html><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="meca.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->
 <TITLE>Mecanografía colocacion de las manos.</TITLE>
<meta http-equiv="Content-Type" content="text/html; ISO-8859-1">
<META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
<META NAME="AUTHOR" CONTENT="Felix Rojo Trueba">
<META NAME="REPLY-TO" CONTENT="felix@cajon.es">
<LINK REV="made" href="mailto:felix@cajon.es">
<META NAME="DESCRIPTION" CONTENT="Colocación de los dedos en el teclado y las teclas que pulsan cada dedo.">
<META NAME="KEYWORDS" CONTENT="teclado,mecanografia,situacion, mapa teclado">
<META NAME="Resource-type" CONTENT="Document">
<META NAME="DateCreated" CONTENT="Mon, 26 May 2014 00:00:00 GMT+1">
<META NAME="robots" content="ALL">
 <!-- InstanceEndEditable -->
 <!-- icono -->
<LINK REL="Shortcut Icon" HREF="imagenes/icono.ico">
</head>
 <body>
 <div itemscope itemtype="http://schema.org/Person"id="todo">
   <div id="cabecera"><header><img src="imagenes/baner.jpg" width="1000" height="150" alt="Meca"></header></div>
   <br>
<div id="menu"><nav>
    <?php
    if(!isset($_SESSION['usuario']))
    {
    echo'
       <form name="form1" method="post" action="loguin_usuario.php">
    Usuario
           <input name="login" type="text" id="login" size="10">
    Contraseña
    <input name="contra"  type="password" id="contra" size="10">
    <input type="submit" name="Enviar" id="Enviar" value="Enviar">
    <a href="registro_de_usuarios.php">Registrate </a> // Recordar contraseña
    ';
    }else{
    echo'Gracias ' . htmlspecialchars($_SESSION['nombre']) . ' <a href="salida_usuario.php">Salir </a> // <a href="historico.php">Historico </a>';
    }
    ?>
<p><a href="index.php">Inicio</a> // <a href="postura_sentarse_ordenador.php">Postura</a> // <a href="mecanografia_colocacion_dedos.php">Colocación de los dedos</a> // <a href="lecciones_mecanografia.php">lecciones</a> // <a href="textos_mecanografia.php">Textos</a> // <a href="curso_de_mecanografia.php">Curso</a> // <a href="contacto.php">Contacto</a></p>
     <p>&nbsp;</p>
   </form>
   </nav></div>
   <div id="cuerpo"><section><!-- InstanceBeginEditable name="EditRegion1" -->
    <script src="jquery-1.11.1.js"></script>
    <div id="present2"><center>
        <h2>Pulsa los botones para ver </h2>
        <p>
  <input type="button" name="dedos" id="dedos"  value="Colocación de los dedos"> 
          <input type="button" name="teclas" id="teclas" value="Teclas que pulsa cada dedo">
        </p>
    </center></div><center><p>
         <?php 
     include "teclado.php";
	 ?><br>
     
	
       <img src="imagenes/colocacion_manos.jpg" width="448" height="188" alt="colocacion de los dedos">
       <p><div id="present2">
       
       
       <h2>La colocación de los dedos en la fila central</h2>
       <p> Mano Izquierda: Meñique A, Anular S, Medio D, Indice F, Pulgar espacio<br>
       Mano Derecha: Meñique Ñ, Anular L, Medio K, Indice J, Pulgar espacio       </p></div><br>

    </center> 
		<script> 
		$(document).on('ready',function (){
			$("#dedos").click(function(){
				// amarillo #FFFF00
				// azul #3399FF
				//verde claro #33FFCC
				//rojo rosado #FF6699
				//verde oscuro #00CC66
				
				
				//
				$("#3tecla2").css({'background-color':'#CCC',});
				$("#3tecla1").css({'background-color':'#CCC',});
				$("#2tecla2").css({'background-color':'#CCC',});
				$("#2tecla1").css({'background-color':'#CCC',});
				$("#1tecla2").css({'background-color':'#CCC',});
				$("#1tecla1").css({'background-color':'#CCC',});
				$("#4tecla3").css({'background-color':'#CCC',});
				$("#4tecla2").css({'background-color':'#CCC',});
				$("#4tecla1").css({'background-color':'#CCC',});
				$("#5tecla2").css({'background-color':'#CCC',});
				$("#5tecla1").css({'background-color':'#CCC',});
				
				//2 dedo
				$("#1tecla3").css({'background-color':'#CCC',});
				$("#2tecla3").css({'background-color':'#CCC',});
				$("#3tecla3").css({'background-color':'#CCC',});
				$("#4tecla4").css({'background-color':'#CCC',});
				$("#5tecla3").css({'background-color':'#CCC',});
				
				//3dedo
				$("#1tecla4").css({'background-color':'#CCC',});
				$("#2tecla4").css({'background-color':'#CCC',});
				$("#3tecla4").css({'background-color':'#CCC',});
				$("#4tecla5").css({'background-color':'#CCC',});
				
				//4 dedo
				$("#1tecla5").css({'background-color':'#CCC',});
				$("#2tecla5").css({'background-color':'#CCC',});
				$("#3tecla5").css({'background-color':'#CCC',});
				$("#4tecla6").css({'background-color':'#CCC',});
				$("#1tecla6").css({'background-color':'#CCC',});
				$("#2tecla6").css({'background-color':'#CCC',});
				$("#3tecla6").css({'background-color':'#CCC',});
				$("#4tecla7").css({'background-color':'#CCC',});
				
				//5 dedo
				$("#1tecla7").css({'background-color':'#CCC',});
				$("#2tecla7").css({'background-color':'#CCC',});
				$("#3tecla7").css({'background-color':'#CCC',});
				$("#4tecla8").css({'background-color':'#CCC',});
				$("#1tecla8").css({'background-color':'#CCC',});
				$("#2tecla8").css({'background-color':'#CCC',});
				$("#3tecla8").css({'background-color':'#CCC',});
				$("#4tecla9").css({'background-color':'#CCC',});
				
				//6 dedo
				$("#1tecla9").css({'background-color':'#CCC',});
				$("#2tecla9").css({'background-color':'#CCC',});
				$("#3tecla9").css({'background-color':'#CCC',});
				$("#4tecla10").css({'background-color':'#CCC',});
				
				//7 dedo
				$("#1tecla10").css({'background-color':'#CCC',});
				$("#2tecla10").css({'background-color':'#CCC',});
				$("#3tecla10").css({'background-color':'#CCC',});
				$("#4tecla11").css({'background-color':'#CCC',});
				
				//8 dedo
				$("#1tecla11").css({'background-color':'#CCC',});
				$("#2tecla11").css({'background-color':'#CCC',});
				$("#3tecla11").css({'background-color':'#CCC',});
				$("#4tecla12").css({'background-color':'#CCC',});
				
				$("#1tecla12").css({'background-color':'#CCC',});
				$("#2tecla12").css({'background-color':'#CCC',});
				$("#3tecla12").css({'background-color':'#CCC',});
				$("#4tecla12").css({'background-color':'#CCC',});
				$("#5tecla6").css({'background-color':'#CCC',});
				
				$("#1tecla13").css({'background-color':'#CCC',});
				$("#2tecla13").css({'background-color':'#CCC',});
				$("#3tecla13").css({'background-color':'#CCC',});
				$("#4tecla13").css({'background-color':'#CCC',});
				$("#5tecla7").css({'background-color':'#CCC',});
				
				$("#1tecla14").css({'background-color':'#CCC',});
				$("#2tecla14").css({'background-color':'#CCC',});
				$("#3tecla14").css({'background-color':'#CCC',});
				$("#5tecla8").css({'background-color':'#CCC',});
								
				//pulgares
				$("#5tecla4").css({'background-color':'#CCC',});
				$("#5tecla5").css({'background-color':'#CCC',});
				//
				
				
				
				$("#3tecla2").css({'background-color':'#FFFF00',});
				$("#3tecla3").css({'background-color':'#3399FF',});
				$("#3tecla4").css({'background-color':'#33FFCC',});
				$("#3tecla5").css({'background-color':'#FF6699',});
				$("#3tecla8").css({'background-color':'#F781F3',});
				$("#3tecla9").css({'background-color':'#33FFCC',});
				$("#3tecla10").css({'background-color':'#3399FF',});
				$("#3tecla11").css({'background-color':'#FFFF00',});
				});
			
			$("#teclas").click(function(){
				// amarillo #FFFF00
				// azul #3399FF
				//verde claro #33FFCC
				//rojo rosado #FF6699
				//rosa #F781F3
				//verde oscuro #00CC66
				//1 dedo
				$("#3tecla2").css({'background-color':'#FFFF00',});
				$("#3tecla1").css({'background-color':'#FFFF00',});
				$("#2tecla2").css({'background-color':'#FFFF00',});
				$("#2tecla1").css({'background-color':'#FFFF00',});
				$("#1tecla2").css({'background-color':'#FFFF00',});
				$("#1tecla1").css({'background-color':'#FFFF00',});
				$("#4tecla3").css({'background-color':'#FFFF00',});
				$("#4tecla2").css({'background-color':'#FFFF00',});
				$("#4tecla1").css({'background-color':'#FFFF00',});
				$("#5tecla2").css({'background-color':'#FFFF00',});
				$("#5tecla1").css({'background-color':'#FFFF00',});
				
				//2 dedo
				$("#1tecla3").css({'background-color':'#3399FF',});
				$("#2tecla3").css({'background-color':'#3399FF',});
				$("#3tecla3").css({'background-color':'#3399FF',});
				$("#4tecla4").css({'background-color':'#3399FF',});
				$("#5tecla3").css({'background-color':'#3399FF',});
				
				//3dedo
				$("#1tecla4").css({'background-color':'#33FFCC',});
				$("#2tecla4").css({'background-color':'#33FFCC',});
				$("#3tecla4").css({'background-color':'#33FFCC',});
				$("#4tecla5").css({'background-color':'#33FFCC',});
				
				//4 dedo
				$("#1tecla5").css({'background-color':'#FF6699',});
				$("#2tecla5").css({'background-color':'#FF6699',});
				$("#3tecla5").css({'background-color':'#FF6699',});
				$("#4tecla6").css({'background-color':'#FF6699',});
				$("#1tecla6").css({'background-color':'#FF6699',});
				$("#2tecla6").css({'background-color':'#FF6699',});
				$("#3tecla6").css({'background-color':'#FF6699',});
				$("#4tecla7").css({'background-color':'#FF6699',});
				
				//5 dedo
				$("#1tecla7").css({'background-color':'#F781F3',});
				$("#2tecla7").css({'background-color':'#F781F3',});
				$("#3tecla7").css({'background-color':'#F781F3',});
				$("#4tecla8").css({'background-color':'#F781F3',});
				$("#1tecla8").css({'background-color':'#F781F3',});
				$("#2tecla8").css({'background-color':'#F781F3',});
				$("#3tecla8").css({'background-color':'#F781F3',});
				$("#4tecla9").css({'background-color':'#F781F3',});
				
				//6 dedo
				$("#1tecla9").css({'background-color':'#33FFCC',});
				$("#2tecla9").css({'background-color':'#33FFCC',});
				$("#3tecla9").css({'background-color':'#33FFCC',});
				$("#4tecla10").css({'background-color':'#33FFCC',});
				
				//7 dedo
				$("#1tecla10").css({'background-color':'#3399FF',});
				$("#2tecla10").css({'background-color':'#3399FF',});
				$("#3tecla10").css({'background-color':'#3399FF',});
				$("#4tecla11").css({'background-color':'#3399FF',});
				
				//8 dedo
				$("#1tecla11").css({'background-color':'#FFFF00',});
				$("#2tecla11").css({'background-color':'#FFFF00',});
				$("#3tecla11").css({'background-color':'#FFFF00',});
				$("#4tecla12").css({'background-color':'#FFFF00',});
				
				$("#1tecla12").css({'background-color':'#FFFF00',});
				$("#2tecla12").css({'background-color':'#FFFF00',});
				$("#3tecla12").css({'background-color':'#FFFF00',});
				$("#4tecla12").css({'background-color':'#FFFF00',});
				$("#5tecla6").css({'background-color':'#FFFF00',});
				
				$("#1tecla13").css({'background-color':'#FFFF00',});
				$("#2tecla13").css({'background-color':'#FFFF00',});
				$("#3tecla13").css({'background-color':'#FFFF00',});
				$("#4tecla13").css({'background-color':'#FFFF00',});
				$("#5tecla7").css({'background-color':'#FFFF00',});
				
				$("#1tecla14").css({'background-color':'#FFFF00',});
				$("#2tecla14").css({'background-color':'#FFFF00',});
				$("#3tecla14").css({'background-color':'#FFFF00',});
				$("#5tecla8").css({'background-color':'#FFFF00',});
								
				//pulgares
				$("#5tecla4").css({'background-color':'#00CC66',});
				$("#5tecla5").css({'background-color':'#00CC66',});
				
				});
				
			});
     	
     	</script>
   <!-- InstanceEndEditable --></div>
   <div id="pie"><footer><div itemscope itemtype="http://schema.org/Person">Programado por: <span itemprop="name">Félix Rojo</span>, Contacto: <span itemprop="email">felix@cajon.es</span></div></footer></div>
 </div>
  
 </body>
<!-- InstanceEnd --></html>