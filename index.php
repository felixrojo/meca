<?php
session_start();
?>
<!DOCTYPE html>
<html><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="meca.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->
 <TITLE>Curso Mecanografía on-line.</TITLE>
<meta http-equiv="Content-Type" content="text/html; ISO-8859-1">
<META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
<META NAME="AUTHOR" CONTENT="Felix Rojo Trueba">
<META NAME="REPLY-TO" CONTENT="felix@cajon.es">
<LINK REV="made" href="mailto:felix@cajon.es">
<META NAME="DESCRIPTION" CONTENT="Curso gratuito de mecanografía online, con seguimiento de cada usuario personalizado conseguirás dominar tu teclado en pocos días..">
<META NAME="KEYWORDS" CONTENT="teclado,mecanografia,curso,test">
<META NAME="Resource-type" CONTENT="Document">
<META NAME="DateCreated" CONTENT="Mon, 26 May 2014 00:00:00 GMT+1">
<META NAME="robots" content="ALL">
 <!-- InstanceEndEditable -->
 <!-- icono -->
<LINK REL="Shortcut Icon" HREF="imagenes/icono.ico">
</head>
 <body>
 <div itemscope itemtype="http://schema.org/Person"id="todo">
   <div id="cabecera"><header><img src="imagenes/baner.jpg" width="1000" height="150" alt="Meca"></header></div>
   <br>
<div id="menu"><nav>
    <?php
    if(!isset($_SESSION['usuario']))
    {
    echo'
       <form name="form1" method="post" action="loguin_usuario.php">
    Usuario
           <input name="login" type="text" id="login" size="10">
    Contraseña
    <input name="contra"  type="password" id="contra" size="10">
    <input type="submit" name="Enviar" id="Enviar" value="Enviar">
    <a href="registro_de_usuarios.php">Registrate </a> // Recordar contraseña
    ';
    }else{
    echo'Gracias ' . htmlspecialchars($_SESSION['nombre']) . ' <a href="salida_usuario.php">Salir </a> // <a href="historico.php">Historico </a>';
    }
    ?>
<p><a href="index.php">Inicio</a> // <a href="postura_sentarse_ordenador.php">Postura</a> // <a href="mecanografia_colocacion_dedos.php">Colocación de los dedos</a> // <a href="lecciones_mecanografia.php">lecciones</a> // <a href="textos_mecanografia.php">Textos</a> // <a href="curso_de_mecanografia.php">Curso</a> // <a href="contacto.php">Contacto</a></p>
     <p>&nbsp;</p>
   </form>
   </nav></div>
   <div id="cuerpo"><section><!-- InstanceBeginEditable name="EditRegion1" -->
   <center><?php
   include 'teclado.php';
   ?></center><br>
<br>

    <div id="present">
     <center><h1>Curso mecanografía on-line gratuito</h1></center>
     <h2>Función.</h2>
     <p class="parrafo2">Esta Web esta creada para aprender o mejorar tu nivel de mecanografía, para que todo el que lo desee tenga acceso a ella.<br>
       Se a diseñado para que se pueda tener un acceso directo a las lecciones para practicas rápidas o si lo prefieren pueden registrarse y empezar un curso en el cual se irán avanzando por las lecciones poco a poco para ir mejorando el nivel. </p>
     <h2>Privacidad.</h2>
     <p class="parrafo2">Para usuarios registrados se  piden los datos mínimos ya que se trata de tener un seguimiento no de capturar datos, por lo que pedimos Usuario y contraseña para acceder a vuestra cuenta, nombre para que la Web os ponga el nombre y el correo electrónico por si queréis recordar la contraseña, el nombre si no os interesa poner otro y el correo electrónico no ponemos activaríamos si queréis mentir o dejarlo en blanco, eso si, si no ponéis el correo electrónico no podréis recuperar la contraseña en caso de olvidarla.</p>
     <h2>Objetivos.</h2>
    <p class="parrafo2">Con esta Web podéis aprender mecanografía en pocos días y la practica os dará más pulsaciones por minuto y recordar practicar y practicar.</p></div> <br>

   <!-- InstanceEndEditable --></div>
   <div id="pie"><footer><div itemscope itemtype="http://schema.org/Person">Programado por: <span itemprop="name">Félix Rojo</span>, Contacto: <span itemprop="email">felix@cajon.es</span></div></footer></div>
 </div>
  
 </body>
<!-- InstanceEnd --></html>