<?php
session_start();
?>
<!DOCTYPE html>
<html><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="meca.css" rel="stylesheet" type="text/css">
 <!-- InstanceBeginEditable name="head" -->
  <TITLE>Contacto Félix Rojo.</TITLE>
<meta http-equiv="Content-Type" content="text/html; ISO-8859-1">
<META NAME="DC.Language" SCHEME="RFC1766" CONTENT="Spanish">
<META NAME="AUTHOR" CONTENT="Felix Rojo Trueba">
<META NAME="REPLY-TO" CONTENT="felix@cajon.es">
<LINK REV="made" href="mailto:felix@cajon.es">
<META NAME="DESCRIPTION" CONTENT="Un poco sobre mi, la manera de ponerse en contacto conmigo.">
<META NAME="KEYWORDS" CONTENT="Felix Rojo,contacto, historia">
<META NAME="Resource-type" CONTENT="Document">
<META NAME="DateCreated" CONTENT="Mon, 26 May 2014 00:00:00 GMT+1">
<META NAME="robots" content="ALL">
 <!-- InstanceEndEditable -->
 <!-- icono -->
<LINK REL="Shortcut Icon" HREF="imagenes/icono.ico">
</head>
 <body>
 <div itemscope itemtype="http://schema.org/Person"id="todo">
   <div id="cabecera"><header><img src="imagenes/baner.jpg" width="1000" height="150" alt="Meca"></header></div>
   <br>
<div id="menu"><nav>
    <?php
    if(!isset($_SESSION['usuario']))
    {
    echo'
       <form name="form1" method="post" action="loguin_usuario.php">
    Usuario
           <input name="login" type="text" id="login" size="10">
    Contraseña
    <input name="contra"  type="password" id="contra" size="10">
    <input type="submit" name="Enviar" id="Enviar" value="Enviar">
    <a href="registro_de_usuarios.php">Registrate </a> // Recordar contraseña
    ';
    }else{
    echo'Gracias ' . htmlspecialchars($_SESSION['nombre']) . ' <a href="salida_usuario.php">Salir </a> // <a href="historico.php">Historico </a>';
    }
    ?>
<p><a href="index.php">Inicio</a> // <a href="postura_sentarse_ordenador.php">Postura</a> // <a href="mecanografia_colocacion_dedos.php">Colocación de los dedos</a> // <a href="lecciones_mecanografia.php">lecciones</a> // <a href="textos_mecanografia.php">Textos</a> // <a href="curso_de_mecanografia.php">Curso</a> // <a href="contacto.php">Contacto</a></p>
     <p>&nbsp;</p>
   </form>
   </nav></div>
   <div id="cuerpo"><section><!-- InstanceBeginEditable name="EditRegion1" --><section><div id="present">
     <center>
       <h1 itemprop="name">Félix Rojo</h1></center>
     <h2>Sobre mi.</h2>
     <div id="foto">
     <img itemprop="image" src="imagenes/fotperfil.jpg" width="160" height="160" alt="Felix Rojo"></div>
     <p class="parrafo2">       Empecé con un Comodore vic 20 con 3k de memoria luego pase al famoso Spectrum 16k y poco a poco he ido cambiando de ordenador y explotando cada uno de ellos lo mejor que podido.</p>
     <p class="parrafo2">En programación empece en Basic cuando la programación era secuencial se numeraban las lineas y se usaba el GOTO para los saltos, poco a poco he ido aprendiendo una cosa de aquí y otra de allí y aprendiendo mas lenguajes e intentar sacar el máximo jugo de mi programación. </p>
<h2>&nbsp;</h2>
<h2>Contacto.</h2>
     <p class="parrafo2">Para Contactar conmigo la forma más común es a través de mi coreo electrónico felix@cajon.es, pero a continuación les dejo un formulario de contacto.</p>
     <p >Puede ponerse en contacto a través de este formulario.
        <form id="form2" name="form2" method="post" action="contacto_enviado.php">
          
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="21%">Email</td>
              <td width="79%" align="left"><label for="email"></label>
              <input type="text" name="email" id="email" /></td>
            </tr>
            <tr>
              <td>Repetir email</td>
              <td align="left"><label for="email2"></label>
              <input type="text" name="email2" id="email2" /></td>
            </tr>
            <tr>
              <td>Mensaje</td>
              <td align="left"><label for="mensaje"></label>
              <textarea name="mensaje" id="mensaje" cols="45" rows="5"></textarea></td>
            </tr>
            
            <tr>
              <td align="left">&nbsp;</td>
              <td align="left"><input type="submit" name="enviar2" id="enviar2" value="Enviar" /></td>
            </tr>
          </table>
        </form>
     </p>
     <h2>Objetivo.</h2>
    <p class="parrafo2">Con esta Web mi objetivo es practicar un poco de programación y de paso si puedo ayudar a que alguna persona aprenda algo.</p></div> <br>
   </section><!-- InstanceEndEditable --></div>
   <div id="pie"><footer><div itemscope itemtype="http://schema.org/Person">Programado por: <span itemprop="name">Félix Rojo</span>, Contacto: <span itemprop="email">felix@cajon.es</span></div></footer></div>
 </div>
  
 </body>
<!-- InstanceEnd --></html>